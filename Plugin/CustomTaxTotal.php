<?php

namespace Mbs\TaxAdditionalAdjustment\Plugin;

class CustomTaxTotal
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $_productloader;
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $_cart;

    private $customTax = null;
    /**
     * @var \Mbs\TaxAdditionalAdjustment\Model\ProductCartItemLoader
     */
    private $cartItemLoader;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Mbs\TaxAdditionalAdjustment\Model\ProductCartItemLoader $cartItemLoader
    ) {
        $this->logger = $logger;
        $this->cartItemLoader = $cartItemLoader;
    }

    private function calculateCustomTaxForQuote()
    {
        if (is_null($this->customTax)) {
            $this->logger->info('--total--');
            $this->customTax = $this->cartItemLoader->getCartItemWithTaxRates();
        }

        return $this->customTax;
    }

    public function beforeAddTotalAmount(
        \Magento\Quote\Model\Quote\Address\Total $subject,
        $code,
        $amount
    ) {
        if ($code =='tax') {
            $amount += $this->calculateCustomTaxForQuote();
        }

        return [$code, $amount];
    }

    public function beforeAddBaseTotalAmount(
        \Magento\Quote\Model\Quote\Address\Total $subject,
        $code,
        $amount
    ) {
        if ($code =='tax') {
            $amount += $this->calculateCustomTaxForQuote();
        }

        return [$code, $amount];
    }
}
