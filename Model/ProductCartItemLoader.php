<?php

namespace Mbs\TaxAdditionalAdjustment\Model;

class ProductCartItemLoader
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;
    /**
     * @var \Magento\Tax\Model\Calculation
     */
    private $taxCalculation;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    private $dataObjectFactory;

    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Tax\Model\Calculation $taxCalculation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory
    ) {
        $this->productFactory = $productFactory;
        $this->cart = $cart;
        $this->customerSession = $customerSession;
        $this->taxCalculation = $taxCalculation;
        $this->scopeConfig = $scopeConfig;
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    public function getCartItemWithTaxRates()
    {
        $itemsCollection = $this->cart->getQuote()->getItemsCollection();
        $productCollection = $this->getProductWithExternalData($itemsCollection);

        $taxAmount = 0;
        foreach ($itemsCollection as $item) {
            $product = $productCollection->getItemByColumnValue('entity_id', $item->getProductId());

            if ($product and $product->getData('pod_image_id')!='') {
                $taxRate = $this->getRate($product);

                if ($taxRate > 0) {
                    $taxAmount += ($product->getData('pod_price') * $taxRate)/100;
                }
            }
        }

        return $taxAmount;
    }

    private function getRate($product)
    {
        $productTaxClassId = $product->getTaxClassId();
        $shippingAddress = $this->customerSession->getCustomer()->getDefaultShippingAddress();
        $countryCode =  (isset($shippingAddress['country_id']) && !empty($shippingAddress['country_id'])) ? $shippingAddress['country_id'] : 'GB';
        $rate = $this->taxCalculation->getRate(
            new \Magento\Framework\DataObject(
                [
                    'country_id' => $countryCode,
                    'customer_class_id' => $this->scopeConfig->getValue('tax/classes/default_customer_tax_class'),
                    'product_class_id' => $productTaxClassId
                ]
            )
        );

        return $rate;
    }

    /**
     * @param $itemsCollection
     * @return \Magento\Framework\Data\Collection
     * @throws \Exception
     */
    private function getProductWithExternalData($itemsCollection):\Magento\Framework\Data\Collection
    {
        $parsedCollection = $this->collectionFactory->create();

        if ($this->cart->getQuote()->getId()) {
            $productIds = [];
            foreach ($itemsCollection as $item) {
                $productIds[] = $item->getProductId();
            }

            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
            $productCollection = $this->productFactory->create()->getCollection()
                ->addFieldToSelect('tax_class_id')
                ->addIdFilter($productIds);

            $productCollection->getSelect()->joinLeft(
                ['pod' => $productCollection->getSelect()->getConnection()->getTableName('pod_temp_cart')],
                'e.entity_id=pod.product_id and pod.quote_id=' . $this->cart->getQuote()->getId(),
                ['pod.image_id as pod_image_id', 'pod.price as pod_price']
            );

            $result = $productCollection->getSelect()->getConnection()->fetchAll($productCollection->getSelect());

            if (!empty($result)) {
                foreach ($result as $item) {
                    $data = $this->dataObjectFactory->create();
                    $data->setData($item);
                    $parsedCollection->addItem($data);
                }
            }
        }

        return $parsedCollection;
    }
}
